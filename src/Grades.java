public class Grades {
    private float algebraGrade;
    private float geometryGrade;
    private float literatureGrade;
    private float historyGrade;
    private float itGrade;

    public Grades(float algebraGrade, float geometryGrade, float literatureGrade, float historyGrade, float itGrade) {
        this.algebraGrade = algebraGrade;
        this.geometryGrade = geometryGrade;
        this.literatureGrade = literatureGrade;
        this.historyGrade = historyGrade;
        this.itGrade = itGrade;
    }

    public float getAlgebraGrade() {
        return algebraGrade;
    }

    public void setAlgebraGrade(float algebraGrade) {
        this.algebraGrade = algebraGrade;
    }

    public float getGeometryGrade() {
        return geometryGrade;
    }

    public void setGeometryGrade(float geometryGrade) {
        this.geometryGrade = geometryGrade;
    }

    public float getLiteratureGrade() {
        return literatureGrade;
    }

    public void setLiteratureGrade(float literatureGrade) {
        this.literatureGrade = literatureGrade;
    }

    public float getHistoryGrade() {
        return historyGrade;
    }

    public void setHistoryGrade(float historyGrade) {
        this.historyGrade = historyGrade;
    }

    public float getItGrade() {
        return itGrade;
    }

    public void setItGrade(float itGrade) {
        this.itGrade = itGrade;
    }
}
