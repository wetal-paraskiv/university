import java.sql.SQLException;
import java.util.Scanner;

public class RunApp {
    public static final Scanner scanner = new Scanner(System.in);

    public static void run() throws SQLException, ClassNotFoundException {
        boolean run = true;
        while (run) {
            System.out.println("Welcome :),\nmake operation choice: ");
            System.out.println("\t(1) new student registration\n" +
                               "\t(2) display all students\n" +
                               "\t(3) display students with average-grade >= ?:\n" +
                               "\t(4) display one student by id\n" +
                               "\t(5) delete one student by id");
            String option = scanner.nextLine();
            switch (option) {
                case "1" -> getDataForNewStudent();
                case "2" -> CRUDStudent.printResultSet("");
                case "3" -> CRUDStudent.printResultSetFilteredByAverageGrades(getMinAverageForProcessing());
                case "4" -> CRUDStudent.printResultSet(getIdForProcessing());
                case "5" -> CRUDStudent.deleteStudent(getIdForProcessing());
            }
            if (option.equalsIgnoreCase("1")) {
                CRUDStudent.printResultSet("");
            }

            System.out.println("\nto Quit press (q), or enter to continue");
            String continueGetData = scanner.nextLine();
            if (continueGetData.equalsIgnoreCase("q")) {
                run = false;
            }
        }
    }

    public static void getDataForNewStudent() {
        try {
            System.out.println("Provide data for students");

            System.out.println("enter full name of the student: ");
            String name = scanner.nextLine();

            System.out.println("enter algebra grade:");
            int algebraGrade = scanner.nextInt();

            System.out.println("enter geometry grade:");
            int geometryGrade = scanner.nextInt();

            System.out.println("enter literature grade:");
            int literatureGrade = scanner.nextInt();

            System.out.println("enter history grade:");
            int historyGrade = scanner.nextInt();

            System.out.println("enter it grade:");
            int itGrade = scanner.nextInt();
            scanner.nextLine();

            Student student = new Student(name);
            Grades grades = new Grades(algebraGrade, geometryGrade, literatureGrade, historyGrade, itGrade);
            student.setGrades(grades);

            CRUDStudent.newStudent(student);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public static String getIdForProcessing() {
        System.out.println("Enter the ID of the student to process the data..");
        return scanner.nextLine();
    }

    public static String getMinAverageForProcessing() {
        System.out.println("Enter the min required average grade to process the data..");
        return scanner.nextLine();
    }
}
