import java.sql.*;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CRUDStudent {

    private static Connection connection = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    private static final String url = resourceBundle.getString("db.url");
    private static final String user = resourceBundle.getString("db.name");
    private static final String password = resourceBundle.getString("db.password");

    public static void createTableExerciseStudents() {
        try {
            String queryCreateTableStudents = "create table if not exists exercise_students " +
                                              "(id int primary key auto_increment," +
                                              "full_name varchar(30) not null," +
                                              "algebra_grade float not null," +
                                              "geometry_grade float not null," +
                                              "literature_grade float not null," +
                                              "history_grade float not null," +
                                              "it_grade float not null)";

            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
            statement.execute(queryCreateTableStudents);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void printResultSet(String id) throws SQLException, ClassNotFoundException {
        String query;
        if (id.equals("")) {
            query = "select * from exercise_students;";
        } else {
            if (checkForGivenId(id)) {
                query = String.format("select * from exercise_students where id = %1$s", id);
            } else return;
        }
        try {
            // set up the connection to database
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        int columnsNumber = resultSetMetaData.getColumnCount();
        System.out.println("\tdata from students table");
        while (resultSet.next()) {
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = resultSet.getString(i);
                System.out.print(resultSetMetaData.getColumnName(i) + ": " + columnValue);
            }
            System.out.println();
        }
    }

    public static void printResultSetFilteredByAverageGrades(String minAvgGrade) throws SQLException {
        String query = "select * from exercise_students where " +
                       "(algebra_grade + geometry_grade + literature_grade + history_grade + it_grade)/5 >= " + minAvgGrade;
        try {
            // set up the connection to database
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (resultSet != null) {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columnsNumber = resultSetMetaData.getColumnCount();
            System.out.println("\tdata of students with average grade >= " + minAvgGrade);
            while (resultSet.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = resultSet.getString(i);
                    System.out.print(resultSetMetaData.getColumnName(i) + ": " + columnValue);
                }
                System.out.println();
            }
        }
    }

    public static void newStudent(Student student) {
        String queryInsertNewEmployee =
                "INSERT INTO exercise_students " +
                "(full_name, algebra_grade, geometry_grade, literature_grade, history_grade, it_grade) " +
                "VALUES (?, ?, ?, ?, ?, ?);";

        try {
            Connection connection = DriverManager.getConnection(url, user, password);
            PreparedStatement pst = connection.prepareStatement(queryInsertNewEmployee);

            pst.setString(1, String.valueOf(student.getFullName()));
            pst.setFloat(2, student.getGrades().getAlgebraGrade());
            pst.setFloat(3, student.getGrades().getGeometryGrade());
            pst.setFloat(4, student.getGrades().getLiteratureGrade());
            pst.setFloat(5, student.getGrades().getHistoryGrade());
            pst.setFloat(6, student.getGrades().getItGrade());

            pst.executeUpdate();

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(CRUDStudent.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    public static void deleteStudent(String id) {
        String queryDeleteEmployee = "delete from exercise_students where id = ?";
        if (checkForGivenId(id)) {
            try {
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement preparedStatement = connection.prepareStatement(queryDeleteEmployee);
                preparedStatement.setString(1, id);
                preparedStatement.executeUpdate();
                System.out.println("deleted student-info with id " + id + "\n");
                printResultSet("");
            } catch (SQLException | ClassNotFoundException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static boolean checkForGivenId(String id) {
        String query = String.format("select * from exercise_students where id = %1$s", id);
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            if (resultSet.next()) return true;
            System.out.println("provided id is 'NOT FOUND error 404 :('");
            return false;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
}
