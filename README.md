after cloning the project:

1. create a database from Intelij-mySQL-console (to ensure that app links to MySQL database)

2. create a file config.properties with location [out/production/Universitate/config.properties]

db.url=jdbc:mysql://localhost:3306/dataBaseName

db.name=dataBaseUserName

db.password=dataBaseUserPassword

3. provide your dataBaseName, userName, password in that file.
